import React, { useContext } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

import GoogleIcon from "mdi-material-ui/Google";

import GoogleLogin from "react-google-login";

import { SnackBarContext } from "../utils/contexts";
import { call_grpc, set_local_token } from "../utils/grpc";
import get_device_info from "../utils/devices";
const { SmediaLoginInput } = require("../../grpc/session_pb");

const useStyles = makeStyles(theme => ({
    media: {
        margin: theme.spacing(1, 0, 1),
        color: "#0A2042",
        backgroundColor: theme.palette.common.white
    }
}));

export default function Google(props) {
    const classes = useStyles();
    const showSnackBar = useContext(SnackBarContext);

    const responseGoogle = response => {
        let token = response.accessToken;
        let request = new SmediaLoginInput();
        request.setMedia("google");
        request.setAccessToken(token);
        request.setDeviceName(get_device_info());

        call_grpc(
            "socialMediaLogin",
            request,
            handle_social_login_response,
            showSnackBar
        );
    };

    const handleFailure = error => {
        console.log("failure when google login:", error);
    };

    function handle_social_login_response(response) {
        let token = response.getAccessToken();
        let firstLogin = response.getBool();
        if (token) {
            set_local_token(token);
            if (firstLogin) {
                props.history.push("/info");
            } else {
                props.history.push("/consumer");
            }
        } else {
            showSnackBar("Server not responding. Please, try again.");
        }
    }

    return (
        <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_CLIENT}
            onSuccess={responseGoogle}
            onFailure={handleFailure}
            render={renderProps => (
                <Button
                    onClick={renderProps.onClick}
                    variant="contained"
                    fullWidth
                    className={classes.media}
                    startIcon={<GoogleIcon />}
                >
                    GOOGLE
                </Button>
            )}
        />
    );
}
