import React from "react";

import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";

// default size is 40 as metaria-ui documentatoin for circular progress component
export default function Loading({ size = 40, ...props }) {
    return (
        <Box pt={3} {...props}>
            <CircularProgress size={size} />
        </Box>
    );
}
