import React from "react";

const SvgIconsnunet14 = props => (
  <svg
    id="icons_nunet-14_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet-14_svg__linear-gradient"
        x1={230.34}
        y1={95.21}
        x2={505.41}
        y2={324.97}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#d3f1f9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-14_svg__linear-gradient-2"
        x1={184.35}
        y1={79.09}
        x2={192.84}
        y2={354.48}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-14_svg__linear-gradient-3"
        x1={203.09}
        y1={295.06}
        x2={594.89}
        y2={308.41}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-14_svg__linear-gradient-4"
        x1={374.04}
        y1={73.24}
        x2={382.53}
        y2={348.63}
        xlinkHref="#icons_nunet-14_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-14_svg__linear-gradient-5"
        x1={203.31}
        y1={288.75}
        x2={595.1}
        y2={302.09}
        xlinkHref="#icons_nunet-14_svg__linear-gradient-3"
      />
      <linearGradient
        id="icons_nunet-14_svg__linear-gradient-6"
        x1={123.71}
        y1={267.24}
        x2={445.65}
        y2={398.45}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.44} stopColor="#4ac2ba" />
        <stop offset={0.66} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-14_svg__linear-gradient-7"
        x1={202.04}
        y1={326.04}
        x2={593.83}
        y2={339.38}
        xlinkHref="#icons_nunet-14_svg__linear-gradient-3"
      />
      <style>
        {
          ".icons_nunet-14_svg__cls-4{fill:#125aa4}.icons_nunet-14_svg__cls-9{fill:none}"
        }
      </style>
    </defs>
    <path
      d="M388.64 127.65a41.06 41.06 0 00-15.48 3 52.68 52.68 0 001.1-10.72c0-31.94-28.74-57.84-64.19-57.84S245.87 88 245.87 120a54 54 0 00.34 6 42.14 42.14 0 00-5.05-.3 43.13 43.13 0 1030.42 73.7c10.53 13.35 26.43 23.42 44.75 23.42 18.14 0 34.74-9.89 45.28-23a41.12 41.12 0 1027-72.1z"
      fill="url(#icons_nunet-14_svg__linear-gradient)"
    />
    <path
      d="M131.49 164.64h124.64V429.1H131.49a5.67 5.67 0 01-5.67-5.67V170.31a5.67 5.67 0 015.67-5.67z"
      fill="url(#icons_nunet-14_svg__linear-gradient-2)"
    />
    <path
      d="M256.13 164.64l37.44 21.89a10.47 10.47 0 015.18 9V387a10.48 10.48 0 01-3.56 7.87l-39.06 34.23z"
      fill="url(#icons_nunet-14_svg__linear-gradient-3)"
    />
    <circle
      className="icons_nunet-14_svg__cls-4"
      cx={192.31}
      cy={182.67}
      r={6.07}
    />
    <path
      className="icons_nunet-14_svg__cls-4"
      d="M138.82 210.66H245.8v20.57H138.82zM138.82 272.65H245.8v20.57H138.82zM138.82 241.66H245.8v20.57H138.82zM138.82 303.65H245.8v20.57H138.82zM138.82 336.14H245.8v20.57H138.82zM138.82 367.13H245.8v20.57H138.82z"
    />
    <path
      d="M321.36 164.78H446v264.46H321.36a5.67 5.67 0 01-5.67-5.67V170.45a5.67 5.67 0 015.67-5.67z"
      fill="url(#icons_nunet-14_svg__linear-gradient-4)"
    />
    <path
      d="M446 164.78l37.43 21.89a10.48 10.48 0 015.19 9v191.46a10.45 10.45 0 01-3.57 7.87L446 429.24z"
      fill="url(#icons_nunet-14_svg__linear-gradient-5)"
    />
    <circle
      className="icons_nunet-14_svg__cls-4"
      cx={380.91}
      cy={180.93}
      r={6.07}
    />
    <path
      className="icons_nunet-14_svg__cls-4"
      d="M327.36 208.15h106.98v20.57H327.36zM327.36 270.15h106.98v20.57H327.36zM327.36 239.15h106.98v20.57H327.36zM327.36 301.14h106.98v20.57H327.36zM327.36 333.63h106.98v20.57H327.36zM327.36 364.62h106.98v20.57H327.36z"
    />
    <path
      d="M218 198.59h124.6v264.46H218a5.67 5.67 0 01-5.67-5.67V204.26a5.67 5.67 0 015.67-5.67z"
      fill="url(#icons_nunet-14_svg__linear-gradient-6)"
    />
    <path
      d="M342.6 198.59l37.4 21.9a10.45 10.45 0 015.19 9v191.45a10.44 10.44 0 01-3.57 7.87l-39 34.24z"
      fill="url(#icons_nunet-14_svg__linear-gradient-7)"
    />
    <path
      className="icons_nunet-14_svg__cls-4"
      d="M223.95 245.11h106.98v20.57H223.95zM223.95 307.1h106.98v20.57H223.95zM223.95 276.11h106.98v20.57H223.95zM223.95 338.1h106.98v20.57H223.95z"
    />
    <circle
      className="icons_nunet-14_svg__cls-4"
      cx={278.77}
      cy={217.08}
      r={6.07}
    />
    <path
      className="icons_nunet-14_svg__cls-4"
      d="M223.95 370.59h106.98v20.57H223.95zM223.95 401.58h106.98v20.57H223.95z"
    />
    <path
      className="icons_nunet-14_svg__cls-9"
      d="M134.45 4.65h358.89v208.33H134.45z"
    />
    <path
      className="icons_nunet-14_svg__cls-9"
      d="M201.3 164.78l54.83-.14 38.67 22.84a4.48 4.48 0 011.57 1.52c.91 1.5 2.38 3.8 2.38 9.59h16.95v-25.71s2.05-8.1 8.1-8.1h102.51s17.5-3.3 17.5-7.08.47-35.46 0-39.72S346.61 22 342.72 17.75s-113-4.26-122.49-1.89S153 60.77 151.61 61.72s17 61.46 17 61.46z"
    />
  </svg>
);

export default SvgIconsnunet14;
