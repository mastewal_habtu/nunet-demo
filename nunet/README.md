# Nunet Demo

See feasibility of some docker containers for orchestration

# Requirements

Install all dependencies:
```
pip3 install -r requirements.txt
```

Generate protobuf files:
```
./install.sh
```

If docker is not running
```
service docker start
```

# Installation

------------
To build the docker model of this application
```bash
export NUNET_PORT="PORT_NUMBER"
docker build -t nunet --build-arg NUNET_PORT=$NUNET_PORT .
```

Deployment
```bash
docker run -it --name nunet -p $NUNET_PORT:$NUNET_PORT -v /var/run/docker.sock:/var/run/docker.sock -v /imdata:/nunet-demo/session_manager/imdata -v /log:/nunet-demo/session_manager/log nunet 
```

For Testing purpose (in another terminal):
```bash
export NUNET_PORT="PORT_NUMBER"
./install.sh
python3 example_client.py
```

# Running Tests

Testing the orchestrator
```
docker build -t nunet .
docker run -it --name nunet -p 50000:50000 -v /var/run/docker.sock:/var/run/docker.sock nunet
python3 test_organizer.py
```

Testing the Session Manager
```
python3 session_manager/test_db.py
```
