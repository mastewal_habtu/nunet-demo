import React from "react";

const SvgIconsnunet12 = props => (
  <svg
    id="icons_nunet-12_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient"
        x1={151.51}
        y1={375.89}
        x2={552.46}
        y2={368.04}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.44} stopColor="#4ac2ba" />
        <stop offset={0.66} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-12"
        x1={87.12}
        y1={348.52}
        x2={156.54}
        y2={348.52}
        gradientTransform="rotate(30.1 246.533 369.295)"
        xlinkHref="#icons_nunet-12_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-3"
        x1={151.43}
        y1={371.95}
        x2={552.38}
        y2={364.1}
        xlinkHref="#icons_nunet-12_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-4"
        x1={247.88}
        y1={159.11}
        x2={250.02}
        y2={287.96}
        xlinkHref="#icons_nunet-12_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-5"
        x1={248.14}
        y1={174.9}
        x2={249.76}
        y2={272.17}
        xlinkHref="#icons_nunet-12_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-6"
        x1={249.75}
        y1={194.21}
        x2={250.73}
        y2={252.86}
        xlinkHref="#icons_nunet-12_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-2"
        x1={309.59}
        y1={71.13}
        x2={286.92}
        y2={403.72}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-7"
        x1={98.94}
        y1={225.35}
        x2={255.58}
        y2={225.35}
        gradientTransform="matrix(-.68 .73 .73 .68 324.33 -8.98)"
        xlinkHref="#icons_nunet-12_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-8"
        x1={118.55}
        y1={225.35}
        x2={235.97}
        y2={225.35}
        gradientTransform="matrix(-.68 .73 .73 .68 324.33 -8.98)"
        xlinkHref="#icons_nunet-12_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-9"
        x1={142.55}
        y1={223.75}
        x2={211.97}
        y2={223.75}
        gradientTransform="matrix(-.68 .73 .73 .68 324.33 -8.98)"
        xlinkHref="#icons_nunet-12_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-10"
        x1={43.51}
        y1={350.12}
        x2={200.15}
        y2={350.12}
        gradientTransform="rotate(30.1 246.533 369.295)"
        xlinkHref="#icons_nunet-12_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-12_svg__linear-gradient-11"
        x1={63.12}
        y1={350.12}
        x2={180.54}
        y2={350.12}
        gradientTransform="rotate(30.1 246.533 369.295)"
        xlinkHref="#icons_nunet-12_svg__linear-gradient"
      />
      <clipPath id="icons_nunet-12_svg__clip-path-3">
        <path
          className="icons_nunet-12_svg__cls-1"
          d="M61.3 338.22l87.76-49.44 5.16-110.85-64.14 6.96-39.35 97.04 6.36 36.01 4.21 20.28z"
        />
      </clipPath>
      <clipPath id="icons_nunet-12_svg__clip-path">
        <path
          className="icons_nunet-12_svg__cls-1"
          d="M180.38 182.38l69.86 41.15 79.27-41.15-30.74-41.86-83.46 11.77-22.5 18.97-12.43 11.12z"
        />
      </clipPath>
      <clipPath id="icons_nunet-12_svg__clip-path-2">
        <path
          className="icons_nunet-12_svg__cls-1"
          d="M464.71 295.38l-98.29-22.03-36.91-104.64 63.43-11.84 65.66 81.56 4.29 36.32 1.82 20.63z"
        />
      </clipPath>
      <style>{".icons_nunet-12_svg__cls-1{fill:none}"}</style>
    </defs>
    <path
      d="M350.2 371.23a39.1 39.1 0 1039.09-39.1 39.1 39.1 0 00-39.09 39.1zm27.3 0A11.81 11.81 0 11389.29 383a11.83 11.83 0 01-11.79-11.77z"
      fill="url(#icons_nunet-12_svg__linear-gradient)"
    />
    <path
      d="M98.15 345.72v23.83a6.52 6.52 0 006.52 6.52h34.92a44.87 44.87 0 01-.5-4.83 49.18 49.18 0 1198.35 0 48.16 48.16 0 01-.49 4.83h103.68a47.57 47.57 0 01-.5-4.83 49.19 49.19 0 0198.37 0 48.16 48.16 0 01-.49 4.83h37.43a6.52 6.52 0 006.52-6.52v-25.27a5.12 5.12 0 00-5.09-5.11v-19.1a19.14 19.14 0 00-14.5-18.56L396.06 285l-49.3-39.64a87.1 87.1 0 00-54.54-19.19h-88.38A22.72 22.72 0 00188.6 232l-32.12 28.93A78.2 78.2 0 00138.41 285l-24.83 51.43c-3.58-.01-15.48 5.69-15.43 9.29zm137.66-99.14h54.46a72.11 72.11 0 0145.23 15.94l23.61 19-123.3-6.25zm-61.2 25.56l25.17-22.65a11.16 11.16 0 017.51-2.9h8v27.61z"
      fill="url(#icons_nunet-12_svg__linear-gradient-2)"
    />
    <path
      d="M149.15 371.23a39.1 39.1 0 1039.09-39.1 39.1 39.1 0 00-39.09 39.1zm27.29 0a11.8 11.8 0 1111.8 11.8 11.84 11.84 0 01-11.8-11.8z"
      fill="url(#icons_nunet-12_svg__linear-gradient-3)"
    />
    <g
      clipPath="url(#icons_nunet-12_svg__clip-path)"
      strokeWidth={6}
      strokeMiterlimit={10}
      fill="none"
    >
      <circle
        cx={248.95}
        cy={223.53}
        r={61.43}
        stroke="url(#icons_nunet-12_svg__linear-gradient-4)"
      />
      <circle
        cx={248.95}
        cy={223.53}
        r={45.65}
        stroke="url(#icons_nunet-12_svg__linear-gradient-5)"
      />
      <circle
        cx={250.24}
        cy={223.53}
        r={26.33}
        stroke="url(#icons_nunet-12_svg__linear-gradient-6)"
      />
    </g>
    <g
      clipPath="url(#icons_nunet-12_svg__clip-path-2)"
      strokeWidth={4}
      strokeMiterlimit={10}
      fill="none"
    >
      <circle
        cx={367.59}
        cy={274.45}
        r={76.32}
        stroke="url(#icons_nunet-12_svg__linear-gradient-7)"
      />
      <circle
        cx={367.59}
        cy={274.45}
        r={56.71}
        stroke="url(#icons_nunet-12_svg__linear-gradient-8)"
      />
      <circle
        cx={366.42}
        cy={273.35}
        r={32.71}
        stroke="url(#icons_nunet-12_svg__linear-gradient-9)"
      />
    </g>
    <g
      clipPath="url(#icons_nunet-12_svg__clip-path-3)"
      strokeWidth={4}
      strokeMiterlimit={10}
      fill="none"
    >
      <circle
        cx={148.26}
        cy={290.16}
        r={76.32}
        stroke="url(#icons_nunet-12_svg__linear-gradient-10)"
      />
      <circle
        cx={148.26}
        cy={290.16}
        r={56.71}
        stroke="url(#icons_nunet-12_svg__linear-gradient-11)"
      />
      <circle
        cx={149.06}
        cy={288.78}
        r={32.71}
        stroke="url(#icons_nunet-12_svg__linear-gradient-12)"
      />
    </g>
  </svg>
);

export default SvgIconsnunet12;
