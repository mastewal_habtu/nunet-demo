from selenium import webdriver

from pyvirtualdisplay import Display
# display = Display(visible=0, size=(1024,768))
# display = Display(visible=0, size=(1920,1080))
display = Display(visible=0, size=(1920,10000))
display.start()

delay_page_load_timeout = 200
delay_implicit_wait = 200

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox') # required when running as root user. otherwise you would get no sandbox errors.

def before_all(context):
    # context.browser = [webdriver.Firefox(),webdriver.Chrome(chrome_options=chrome_options)]
    context.browser = [webdriver.Chrome(chrome_options=chrome_options)]
    # context.browser = [webdriver.Firefox()]
    for browser in context.browser:
        browser.set_page_load_timeout(delay_page_load_timeout)
        browser.implicitly_wait(delay_implicit_wait)
        browser.maximize_window()

def after_all(context):
    for browser in context.browser:
        browser.quit()
