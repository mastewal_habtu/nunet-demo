import unittest
import logging
import organizer
import time
import sys
import docker
import grpc
import object_detection_pb2_grpc
import object_detection_pb2

import image_recon_pb2_grpc
import image_recon_pb2
import base64
import random

import json
import ast
import requests

from constants import CONTAINER_NAME

import os
from multiprocessing import Process,current_process
# A test script which creates 26 networks (maximum capacity) with each having their own yolo and CNTK containers talking with the nunet
# One needs to clean up docker after running this

def yolo(yolo_arg, yolo_img, img):
        #cpu_percent = 0
        net = orch.client.networks.get(CONTAINER_NAME)
        yolov_cont, ip =orch.create(net.id, yolo_img, yolo_arg)
        start_time = time.time()
        bool, val , stat = orch.call_yolo_cont(ip, yolov_cont, 8889, img)
        #print(stat)
        print("--- %s seconds ---" % (time.time() - start_time))
        stat=orch.cont_stat(yolov_cont)
        print("YOLO Stat")
        print(stat)

def cntk(cntk_arg, cntk_img, img):
        cpu_percent = 0
        net = orch.client.networks.get(CONTAINER_NAME)
        cntk_cont, ip = orch.create(net.id,"cntk-image-recon", cntk_arg)
        start_time = time.time()
        bool, val , stat = orch.call_cntk_cont(ip, cntk_cont, 8890, img)
        #print(stat)
        print("--- %s seconds ---" % (time.time() - start_time))
        stat = orch.cont_stat(cntk_cont)
        print("CONT Stat")
        print(stat)

if __name__ == '__main__':
   orch = organizer.Orchestrator()

   yolo_arg = ["python3", "-m", "service.object_detection_service", "--grpc-port","8889"]
   yolo_img="yolov3-object-detection"

   cntk_arg = ["/root/anaconda3/envs/cntk-py35/bin/python", "-m", "service.image_recon_service", "--grpc-port","8890"]
   cntk_img="cntk-image-recon"

   img =base64.b64encode(requests.get("https://singnet.github.io/dnn-model-services/assets/users_guide/bulldog.jpg").content)

   yolo(yolo_arg, yolo_img, img)
   cntk(cntk_arg, cntk_img, img)
