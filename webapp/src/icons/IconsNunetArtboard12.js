import React from "react";

const SvgIconsnunetArtboard12 = props => (
  <svg
    id="icons_nunet_Artboard_1-2_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet_Artboard_1-2_svg__linear-gradient"
        x1={330.98}
        y1={21.13}
        x2={349.01}
        y2={405.74}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet_Artboard_1-2_svg__linear-gradient-2"
        x1={340.71}
        y1={288.67}
        x2={339.96}
        y2={398.19}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.67} stopColor="#1e9fc2" stopOpacity={0.87} />
        <stop offset={1} stopColor="#2e9ad6" stopOpacity={0.8} />
      </linearGradient>
      <linearGradient
        id="icons_nunet_Artboard_1-2_svg__linear-gradient-3"
        x1={283.9}
        y1={106.37}
        x2={154.05}
        y2={346.81}
        xlinkHref="#icons_nunet_Artboard_1-2_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet_Artboard_1-2_svg__linear-gradient-4"
        x1={368.99}
        y1={421.18}
        x2={-30.67}
        y2={369.55}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet_Artboard_1-2_svg__linear-gradient-5"
        x1={222.34}
        y1={211.07}
        x2={176.03}
        y2={296.82}
        xlinkHref="#icons_nunet_Artboard_1-2_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet_Artboard_1-2_svg__linear-gradient-6"
        x1={240.48}
        y1={102.38}
        x2={487.89}
        y2={317.32}
        xlinkHref="#icons_nunet_Artboard_1-2_svg__linear-gradient-4"
      />
      <linearGradient
        id="icons_nunet_Artboard_1-2_svg__linear-gradient-7"
        x1={223.32}
        y1={122.14}
        x2={470.73}
        y2={337.08}
        xlinkHref="#icons_nunet_Artboard_1-2_svg__linear-gradient-4"
      />
      <linearGradient
        id="icons_nunet_Artboard_1-2_svg__linear-gradient-8"
        x1={269.03}
        y1={69.52}
        x2={516.45}
        y2={284.46}
        xlinkHref="#icons_nunet_Artboard_1-2_svg__linear-gradient-4"
      />
      <clipPath id="icons_nunet_Artboard_1-2_svg__clip-path">
        <path fill="none" d="M151.92 355.46h277.74V494H151.92z" />
      </clipPath>
      <style>
        {
          ".icons_nunet_Artboard_1-2_svg__cls-3{fill:#fff;opacity:.76}.icons_nunet_Artboard_1-2_svg__cls-10{fill:#047a6f}"
        }
      </style>
    </defs>
    <rect
      x={176.47}
      y={79.39}
      width={325.75}
      height={240.26}
      rx={9.92}
      fill="url(#icons_nunet_Artboard_1-2_svg__linear-gradient)"
    />
    <path
      className="icons_nunet_Artboard_1-2_svg__cls-3"
      d="M219.43 94.35h-9.1c-11.69 0-21.16 11-21.16 24.56v161.37c0 13.56 9.47 24.55 21.16 24.55H469c11.69 0 21.17-11 21.17-24.55V118.91c0-13.56-9.48-24.56-21.17-24.56z"
    />
    <rect
      x={166.74}
      y={318.92}
      width={347.35}
      height={25.25}
      rx={8.5}
      fill="url(#icons_nunet_Artboard_1-2_svg__linear-gradient-2)"
    />
    <circle
      className="icons_nunet_Artboard_1-2_svg__cls-3"
      cx={349.1}
      cy={330.83}
      r={8.54}
    />
    <rect
      x={150.45}
      y={197.3}
      width={85.41}
      height={154.22}
      rx={4.85}
      fill="url(#icons_nunet_Artboard_1-2_svg__linear-gradient-3)"
    />
    <path
      className="icons_nunet_Artboard_1-2_svg__cls-3"
      d="M162.23 212.79h62.2a5.39 5.39 0 015.39 5.39v110.51a4.39 4.39 0 01-4.39 4.39h-64.19a4.39 4.39 0 01-4.39-4.39V218.18a5.39 5.39 0 015.38-5.39z"
    />
    <path
      stroke="#c8e4f1"
      strokeLinecap="round"
      strokeMiterlimit={10}
      fill="none"
      d="M183.1 204.89h21.35"
    />
    <circle cx={193.33} cy={341.74} r={6.05} fill="#c8e4f1" />
    <g clipPath="url(#icons_nunet_Artboard_1-2_svg__clip-path)">
      <path
        d="M277 387.58c-9.73-11.38-19.81-25.49-34.44-29.7-7.7-2.22-81.58-.86-90.6.42-8.6 1.22-5.95 68-4.17 67.51a46.72 46.72 0 0125.53-.15c8.66 2.33 17 6.17 25 10.34 8.95 4.67 17.68 9.79 26.47 14.77s17.57 9.83 26.71 13.93c8.84 4 18.47 7.41 28.15 7.54a1.27 1.27 0 01.28 0c9.93-3.32 18.85-9.1 27.69-14.83 4.4-2.85 8.81-5.72 13.37-8.29 3.51-2 18.94-5.66 26.67-6.48 6-.63 14.82-1.39 14.68-9.39a32.85 32.85 0 00-.41-4.54c13.54-9.93 30.4-26 41.32-37.49a40.38 40.38 0 007.19-11.71c1.36-3.71-1.24-8.74-3.8-11.67a10.06 10.06 0 00-8.46-3 22.79 22.79 0 00-6.35 1.65 3.23 3.23 0 01-3.76-1.06 13.15 13.15 0 00-9.95-5.29 12.51 12.51 0 00-7.36 2.25 3.36 3.36 0 01-4.81-1.34 7.91 7.91 0 00-3.4-3.21c-3.19-1.5-7-.82-10.08.83s-5.7 4.2-8.35 6.59c-.07-2.7-2.78-5.13-5.47-5.38a12 12 0 00-7.79 1.85c-15.92 11-35.62 17.84-56.16 23.37"
        fill="url(#icons_nunet_Artboard_1-2_svg__linear-gradient-4)"
      />
      <path
        className="icons_nunet_Artboard_1-2_svg__cls-10"
        d="M246 431.32a34.36 34.36 0 007.26 7.14 38.35 38.35 0 004.28 2.74 43.82 43.82 0 004.6 2.17 51.82 51.82 0 009.76 2.8 51 51 0 0010.12.62c6.79-.1 13.65-1 20.52-.4 3.4.26 6.82.43 10.23.57s6.83.28 10.25.35c-6.84.26-13.68.16-20.53-.15-6.79-.46-13.58.44-20.45.63a51.71 51.71 0 01-10.31-.73 46.55 46.55 0 01-9.88-3 39.38 39.38 0 01-8.88-5.22 32.59 32.59 0 01-6.97-7.52zM369.78 364.61a85.2 85.2 0 01-12.24 12.15c-4.43 3.68-9.11 7.05-13.87 10.28s-9.71 6.19-14.73 9-10.12 5.46-15.37 7.82c2.53-1.38 5.06-2.73 7.54-4.17s5-2.84 7.45-4.32c4.92-2.94 9.8-6 14.55-9.17s9.44-6.51 13.95-10c2.25-1.77 4.45-3.6 6.58-5.52 1.09-.93 2.09-2 3.14-2.94s1.99-2.1 3-3.13zM388.38 370.16a140.27 140.27 0 01-8.78 11.28c-3.1 3.62-6.39 7.08-9.77 10.44a139.71 139.71 0 01-10.76 9.44 71.14 71.14 0 01-12 7.74c1-.65 2-1.25 3-1.91s2-1.32 2.91-2q2.89-2.09 5.62-4.38c3.64-3 7.17-6.21 10.54-9.55s6.68-6.74 9.9-10.24q2.4-2.62 4.73-5.33c1.57-1.83 3.08-3.65 4.61-5.49z"
      />
      <path
        className="icons_nunet_Artboard_1-2_svg__cls-10"
        d="M279 389.61l4.42 4.11a33.21 33.21 0 004.65 3.77 23.36 23.36 0 005.61 2c1.94.56 3.84 1.17 5.8 1.58a80.3 80.3 0 0011.89 1.58c4 .29 8 .33 12.07.64a85.87 85.87 0 0111.93 2.23 71.5 71.5 0 0111.46 3.95A37.22 37.22 0 01357 416a17.49 17.49 0 013.6 4.89 12.48 12.48 0 011.28 5.87 13.05 13.05 0 00-1.48-5.76 17.41 17.41 0 00-3.71-4.65 36.58 36.58 0 00-10.17-6.22 84.41 84.41 0 00-23.18-5.89c-7.95-.6-16.14-.52-24-2.46-2-.47-3.9-1.13-5.8-1.72a23.41 23.41 0 01-5.68-2.19 33 33 0 01-4.58-4c-1.47-1.37-2.87-2.82-4.28-4.26z"
      />
      <path
        className="icons_nunet_Artboard_1-2_svg__cls-10"
        d="M343.18 367.59a74.58 74.58 0 01-12.18 9.48c-4.33 2.79-8.86 5.26-13.44 7.61s-9.3 4.42-14.07 6.36-9.6 3.72-14.52 5.22c2.39-1 4.78-1.89 7.14-2.9s4.74-2 7.08-3c4.69-2.08 9.35-4.21 13.92-6.54s9.09-4.74 13.48-7.37c2.2-1.31 4.36-2.7 6.46-4.17 1.08-.69 2.07-1.5 3.12-2.24s2-1.65 3.01-2.45z"
      />
    </g>
    <path
      d="M213.07 259.91a19.21 19.21 0 10-38.42 0c0 5.1 19.21 48.22 19.21 48.22s19.21-43.13 19.21-48.22z"
      fill="url(#icons_nunet_Artboard_1-2_svg__linear-gradient-5)"
    />
    <circle cx={193.86} cy={260.6} r={15.05} fill="#fff" />
    <path
      d="M450.37 228v-18.84H427.7a53.81 53.81 0 00-8.95-21.71l16-16-13.38-13.34-16 16a54 54 0 00-21.65-9v-22.63h-18.88v22.64a53.87 53.87 0 00-21.69 9l-16-16-13.34 13.34 16 16a53.87 53.87 0 00-9 21.71h-22.62V228h22.64a53.58 53.58 0 009 21.67l-16 16 13.34 13.35 16-16a53.82 53.82 0 0021.69 9v22.67h18.89v-22.64a53.88 53.88 0 0021.65-9l16 16 13.38-13.35-16-16a53.52 53.52 0 008.92-21.7zm-76.09 25.31A34.75 34.75 0 11409 218.6a34.75 34.75 0 01-34.72 34.75z"
      fill="url(#icons_nunet_Artboard_1-2_svg__linear-gradient-6)"
    />
    <path
      d="M315.83 161.6l-2-10.61-12.73 2.45a30.55 30.55 0 00-7.37-11.23L301 131.5l-8.95-6-7.25 10.71a30.89 30.89 0 00-13.14-2.72l-2.44-12.71-10.6 2 2.38 12.7a30.79 30.79 0 00-11.22 7.38l-10.71-7.26-6.07 8.93 10.71 7.27A30.82 30.82 0 00241 165l-12.71 2.44 2 10.6 12.72-2.44a30.62 30.62 0 007.38 11.2l-7.26 10.74 8.94 6.06 7.25-10.72a30.77 30.77 0 0013.15 2.7l2.53 12.69 10.6-2-2.44-12.74a30.68 30.68 0 0011.2-7.37L305 193.4l6.08-8.94-10.72-7.28A30.55 30.55 0 00303.1 164zm-40 22.42a19.88 19.88 0 1115.77-23.27A19.86 19.86 0 01275.82 184z"
      fill="url(#icons_nunet_Artboard_1-2_svg__linear-gradient-7)"
    />
    <path
      d="M363 144.68l2.69-6.17-7.4-3.22a19.06 19.06 0 00.17-8.36l7.5-3-2.47-6.25-7.49 2.99a19.28 19.28 0 00-5.79-6l3.22-7.39-6.16-2.69-3.21 7.41a19.2 19.2 0 00-8.36-.16l-2.94-7.49-6.25 2.45 2.94 7.5a19.27 19.27 0 00-6 5.81l-7.45-3.25-2.64 6.14 7.38 3.22a19.16 19.16 0 00-.15 8.35l-7.5 3 2.45 6.25 7.5-3a18.92 18.92 0 005.81 6l-3.23 7.41 6.16 2.68 3.23-7.4a19.11 19.11 0 008.34.16l2.94 7.5 6.27-2.45-2.94-7.51a19 19 0 006-5.8zm-28.43-2.57a12.37 12.37 0 1116.28-6.4 12.37 12.37 0 01-16.26 6.4z"
      fill="url(#icons_nunet_Artboard_1-2_svg__linear-gradient-8)"
    />
  </svg>
);

export default SvgIconsnunetArtboard12;
