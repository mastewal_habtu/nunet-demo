import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import Container from "@material-ui/core/Container";
import CardMedia from "@material-ui/core/CardMedia";
import CircularProgress from "@material-ui/core/CircularProgress";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";

import ExplanationIcon from "mdi-material-ui/Compass";
import OutputIcon from "mdi-material-ui/WeatherCloudyArrowRight";
import InputIcon from "@material-ui/icons/Input";
import ServiceIcon from "mdi-material-ui/Ungroup";
import ResourceIcon from "@material-ui/icons/Toys";
import RewardIcon from "mdi-material-ui/GiftOutline";
import NunetIcon from "../../icons/IconsNunet10";
import DogIcon from "@material-ui/icons/Pets";
import StartTaskButton from "../commons/StartTask";
import IconTypography from "../commons/IconTypography";
import { UserContext } from "../utils/contexts";
import { render } from "react-dom";

const useStyles = makeStyles(theme => ({
    gutterTop: {
        paddingTop: theme.spacing(2)
    },
    innerContainer: {
        padding: 0
    },
    half_divider: {
        width: "40%",
        margin: "0 auto"
    },
    under_nunet_icon: {
        marginTop: theme.spacing(-3)
    },
    nunet_icon: {
        height: "8rem",
        marginTop: "0rem"
    }
}));

function createData(arch, picture, breed, base64, time, discovered_times) {
    return {
        arch: base64 ? base64 : "architecture/" + arch,
        picture: base64 ? base64 : "breeds/" + picture,
        breed: breed,
        time: time,
        discovered_times: discovered_times + " times"
    };
}

const sampleOutput = createData(
    "nunet_demo_architecture.svg",
    "golden_retriever.jpeg",
    "Golden Retriever",
    null,
    "1 minute ago",
    6
);

export default function TaskInformation(props) {
    const classes = useStyles();

    const [loading, setLoading] = useState(true);
    const [isFirstLogIn, setFirstLogin] = useState(false);
    const [userInfo, setUserInfo] = React.useContext(UserContext);

    const paymentPerTask = 15;

    useEffect(() => {
        if (userInfo === false) {
            setLoading(false);
        } else if (userInfo) {
            setLoading(false);
        }
    }, [userInfo]);

    function goto_reward_table() {
        props.history.push("/rewards");
    }

    return (
        <React.Fragment>
            {loading ? (
                <CircularProgress />
            ) : (
                <React.Fragment>
                    {/**
                     *  Show the initial token award if the user is
                     *  logging in for the first time
                     */
                    isFirstLogIn && (
                        <React.Fragment>
                            <Typography
                                variant="h5"
                                gutterBottom
                                className={classes.gutterTop}
                            >
                                You are awarded
                            </Typography>
                            <Typography variant="h5">
                                {userInfo.getBalance()}NTXd tokens!
                            </Typography>
                        </React.Fragment>
                    )}

                    <NunetIcon className={classes.nunet_icon} />
                    {userInfo && (
                        <Typography
                            variant="h6"
                            gutterBottom
                            className={classes.under_nunet_icon}
                        >
                            Your balance allows to run{" "}
                            {Math.floor(userInfo.getBalance() / paymentPerTask)}{" "}
                            computational tasks on NuNet infrastructure
                        </Typography>
                    )}
                </React.Fragment>
            )}

            <React.Fragment>
                <Container
                    className={classes.innerContainer}
                    style={{ marginTop: 20 }}
                    align="left"
                >
                    <IconTypography
                        variant="h6"
                        gutterBottom
                        icon={ExplanationIcon}
                    >
                        Computational task
                    </IconTypography>
                    <Divider></Divider>
                    <Typography
                        variant="body1"
                        className={classes.gutterTop}
                        gutterBottom
                        align="justify"
                    >
                        NuNet allows to run{" "}
                        <Link
                            href="https://beta.singularitynet.io/"
                            underline="always"
                            target="_blank"
                        >
                            SingularityNET
                        </Link>{" "}
                        services on computing resources
                        that are pooled from independent providers and connect several
                        of them to perform a more complex task.
                        Providers are compensated in NTXd<sup><Link href="#note1">1</Link></sup> tokens.
                        In this demo, you, as a consumer, are given an initial amount of
                        tokens and can initiate AI task execution by submitting a dog
                        picture and the amount of tokens required.
                        If a dog and its breed is recognized in the picture,
                        you are awarded an additional amount of NTXd tokens
                        (depending on the breed of the dog discovered).
                        The full NuNet version will enable computational economy consisting of any
                        processes and hardware devices.
                    </Typography>

                    <Divider></Divider>

                    <object style={{height: 'auto', width: '100%'}} type="image/svg+xml" data={sampleOutput.arch}> </object>

                    <IconTypography
                        variant="h6"
                        gutterBottom
                        icon={ResourceIcon}
                        className={classes.gutterTop}
                    >
                        Resource Requirements
                    </IconTypography>
                    <Divider></Divider>

                    <Box textAlign="center" pt={2} clone>
                        <Grid container spacing={2}>
                            <Grid item sm={3} xs={6}>
                                <Typography variant="body1" gutterBottom>
                                    CPU
                                </Typography>
                                <Divider className={classes.half_divider} />
                                <Box mt={1}>
                                    <Typography variant="body1" gutterBottom>
                                        85 Mticks
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item sm={3} xs={6}>
                                <Typography variant="body1" gutterBottom>
                                    RAM
                                </Typography>
                                <Divider className={classes.half_divider} />
                                <Box mt={1}>
                                    <Typography variant="body1" gutterBottom>
                                        8596 MBs
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item sm={3} xs={6}>
                                <Typography variant="body1" gutterBottom>
                                    NET
                                </Typography>
                                <Divider className={classes.half_divider} />
                                <Box mt={1}>
                                    <Typography variant="body1" gutterBottom>
                                        9 KB
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item sm={3} xs={6}>
                                <Typography variant="body1" gutterBottom>
                                    TIME
                                </Typography>
                                <Divider className={classes.half_divider} />
                                <Box mt={1}>
                                    <Typography variant="body1" gutterBottom>
                                        25 s
                                    </Typography>
                                </Box>
                            </Grid>
                        </Grid>
                    </Box>

                    <IconTypography
                        variant="h6"
                        gutterBottom
                        icon={OutputIcon}
                        className={classes.gutterTop}
                    >
                        Sample Output
                    </IconTypography>
                    <Divider></Divider>

                    <Card>
                        <CardMedia component="img" src={sampleOutput.picture} />
                        <CardContent>
                            <Box px={2} align="center">
                                <IconTypography
                                    variant="h6"
                                    gutterBottom
                                    icon={DogIcon}
                                >
                                    {sampleOutput.breed}
                                </IconTypography>

                                <Typography
                                    variant="subtitle2"
                                    color="textSecondary"
                                    gutterBottom
                                >
                                    99.2% confidence
                                </Typography>
                            </Box>
                        </CardContent>
                    </Card>
                </Container>

                <Grid
                    container
                    className={classes.gutterTop}
                    spacing={2}
                    justify="space-evenly"
                    alignItems="center"
                >
                    <Grid item xs={6}>
                        <Button
                            variant="contained"
                            color="primary"
                            startIcon={<RewardIcon />}
                            onClick={goto_reward_table}
                        >
                            Reward List
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <StartTaskButton history={props.history} />
                    </Grid>
                </Grid>
                <Grid>
                    <p id="note1"><sup>1</sup>
                    NTXd tokens are designated for the didactic purposes within the
                    scope of this demo only. NTXd are not implemented on any blockchain
                    and will not be implemented in the future. The actual native
                    tokens NTX will be implemented as per NuNet platform development strategy independently of this demo.
                    </p>
                </Grid>
            </React.Fragment>
        </React.Fragment>
    );
}
