FROM ubuntu:18.04

RUN apt-get update && apt-get install -y software-properties-common
RUN add-apt-repository ppa:deadsnakes/ppa -y

ARG NUNET_PORT
ENV NUNET_PORT=$NUNET_PORT
ENV NUNET_CONTAINER_NAME=dev_nunet

RUN apt-get update && apt-get install -y --no-install-recommends \
        git \
        build-essential \
        python3.6 \
        python3.6-dev \
        python3-pip \
        python-setuptools \
        cmake \
        wget \
        curl \
        libsm6 \
        libxext6 \
        libxrender-dev \
        nano

EXPOSE $NUNET_PORT

COPY . /nunet-demo
COPY requirements.txt  /tmp
RUN curl https://bootstrap.pypa.io/get-pip.py | python3.6
RUN python3 -m pip install -r /tmp/requirements.txt
WORKDIR /nunet-demo/session_manager
RUN chmod +x ../install.sh
RUN cd .. && ./install.sh

# Needed for running system tests

WORKDIR /nunet-demo/system_test

RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install && \
    apt -y install firefox && \
    wget https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux32.tar.gz && \
    tar -xzf geckodriver-v0.26.0-linux32.tar.gz && \
    mv geckodriver /usr/local/bin/ && \
    wget https://chromedriver.storage.googleapis.com/78.0.3904.70/chromedriver_linux64.zip && \
    apt -y  install unzip && \
    unzip chromedriver_linux64.zip && \
    mv chromedriver /usr/local/bin/ && \
    cp keyboard /etc/default/ && \
    export DEBIAN_FRONTEND="noninteractive" && \
    apt update &&\
    apt -y install xvfb && \
    apt install -y xorg xvfb dbus-x11 xfonts-100dpi xfonts-75dpi xfonts-cyrillic && \
    yes | pip install pyvirtualdisplay behave selenium allure-python-commons



# Needed for running system tests

WORKDIR /nunet-demo/session_manager

EXPOSE 8889
EXPOSE 8890

RUN mkdir /session_db

RUN chmod +x data.sh 
CMD ./data.sh 