import React from "react";

const SvgIconsnunet22 = props => (
  <svg
    id="icons_nunet-22_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-7"
        x1={159.76}
        y1={370.48}
        x2={189.53}
        y2={370.48}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-14"
        x1={174.54}
        y1={328.21}
        x2={316.84}
        y2={328.21}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-2"
        x1={142.25}
        y1={336.67}
        x2={181.26}
        y2={336.67}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-3"
        x1={314.25}
        y1={438.15}
        x2={318.25}
        y2={438.15}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-4"
        x1={455.42}
        y1={329.12}
        x2={496.81}
        y2={329.12}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-5"
        x1={315.89}
        y1={438.15}
        x2={378.56}
        y2={438.15}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-6"
        x1={474.11}
        y1={347.4}
        x2={478.11}
        y2={347.4}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient"
        x1={157.68}
        y1={336.61}
        x2={474.82}
        y2={336.61}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-8"
        x1={181.26}
        y1={397.66}
        x2={203.98}
        y2={397.66}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-9"
        x1={375.58}
        y1={438.15}
        x2={398.3}
        y2={438.15}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-10"
        x1={303.41}
        y1={473.76}
        x2={330.14}
        y2={473.76}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-11"
        x1={494.81}
        y1={329.12}
        x2={521.53}
        y2={329.12}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-12"
        x1={117.53}
        y1={335.61}
        x2={144.25}
        y2={335.61}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-22_svg__linear-gradient-13"
        x1={464.75}
        y1={373.04}
        x2={487.47}
        y2={373.04}
        xlinkHref="#icons_nunet-22_svg__linear-gradient"
      />
      <style>
        {
          ".icons_nunet-22_svg__cls-1{fill:#1259a3}.icons_nunet-22_svg__cls-2{fill:#00a69c}.icons_nunet-22_svg__cls-3{fill:#1182b5}.icons_nunet-22_svg__cls-4{fill:#2e9ad6}"
        }
      </style>
    </defs>
    <path
      className="icons_nunet-22_svg__cls-1"
      d="M301.73 238a21.67 21.67 0 1021.67 21.66A21.67 21.67 0 00301.73 238zm0 31.88A10.22 10.22 0 11312 259.64a10.22 10.22 0 01-10.27 10.22z"
    />
    <path
      className="icons_nunet-22_svg__cls-2"
      d="M409.74 256.33l-22.06 9.9-64.28-6.43a21.88 21.88 0 01-.26 3.05l65.19 17.23L406 292.85c5.24 3.79 10.07 1.62 10.73-4.81l2.55-24.81c.65-6.44-3.64-9.54-9.54-6.9zm1.83 11.83l-1.33 12.89c-.34 3.33-2.85 4.46-5.57 2.49L394.18 276c-2.72-2-2.44-4.69.62-6.06l11.82-5.3c3.06-1.42 5.28.19 4.95 3.52z"
    />
    <path
      className="icons_nunet-22_svg__cls-2"
      d="M404.39 269.76l-5.57 2.49c-1.44.65-1.57 1.93-.29 2.86l4.94 3.57c1.28.93 2.46.4 2.62-1.18l.62-6.06c.16-1.58-.88-2.33-2.32-1.68zM229.78 217.86l8.48.88c2.2.23 3.26-1.24 2.36-3.25l-3.48-7.79c-.9-2-2.7-2.2-4-.42l-5 6.92c-1.3 1.8-.56 3.44 1.64 3.66z"
    />
    <path
      className="icons_nunet-22_svg__cls-2"
      d="M216.16 231.18l30.74 3.19 35.68 15.15a22 22 0 013.1-4.4l-27.56-28.34-12-26.92c-3.66-8.21-11-9-16.25-1.68l-20.33 28.1c-5.31 7.27-2.32 13.98 6.62 14.9zm5.09-16.31l10.55-14.59c2.74-3.77 6.53-3.38 8.44.88l7.34 16.43c1.9 4.25-.33 7.35-5 6.87l-17.91-1.85c-4.61-.48-6.16-3.97-3.42-7.74zM225.62 291.41l-7.1-.91c-1.84-.24-2.76 1-2.05 2.68l2.77 6.61c.72 1.71 2.22 1.9 3.35.42l4.33-5.69c1.13-1.47.54-2.87-1.3-3.11z"
    />
    <path
      className="icons_nunet-22_svg__cls-2"
      d="M281.29 266.78l-44.89 14.5-26.52-3.39c-7.23-.93-10.85 3.82-8 10.55l10.85 26c2.83 6.73 8.74 7.48 13.16 1.68l16.91-22.2 40-23.81a22.45 22.45 0 01-1.51-3.33zm-48.7 27.14l-8.85 11.62c-2.29 3-5.36 2.61-6.82-.87l-5.64-13.48c-1.46-3.49.42-5.95 4.17-5.47l14.49 1.85c3.75.49 4.95 3.34 2.65 6.35z"
    />
    <path
      className="icons_nunet-22_svg__cls-3"
      d="M291 201.6a4.46 4.46 0 10-4.46 4.45 4.46 4.46 0 004.46-4.45z"
    />
    <path
      className="icons_nunet-22_svg__cls-3"
      d="M293.89 239.46a21.85 21.85 0 014.8-1.25l-2.49-21a18.41 18.41 0 10-11 2.69zM275 201.6a11.54 11.54 0 1111.53 11.54A11.54 11.54 0 01275 201.6zM418.33 317.62a4.45 4.45 0 104.45 4.45 4.46 4.46 0 00-4.45-4.45z"
    />
    <path
      className="icons_nunet-22_svg__cls-3"
      d="M418.33 302.79a19.22 19.22 0 00-14.67 6.8l-81.13-43.88a20.52 20.52 0 01-.88 2.46l77.41 53.61a2.83 2.83 0 000 .29 19.29 19.29 0 1019.29-19.28zm0 29.95A10.67 10.67 0 11429 322.07a10.69 10.69 0 01-10.67 10.67zM307.89 295.67L305.07 281a21.6 21.6 0 01-3.34.29 23.35 23.35 0 01-2.52-.16l-2.5 14.29a18.28 18.28 0 1011.18.22zM302 322.59a9.66 9.66 0 119.66-9.66 9.66 9.66 0 01-9.66 9.66z"
    />
    <path
      className="icons_nunet-22_svg__cls-3"
      d="M302 308.48a4.46 4.46 0 104.46 4.45 4.44 4.44 0 00-4.46-4.45z"
    />
    <path
      className="icons_nunet-22_svg__cls-4"
      d="M308.05 186.62l-4.16 51.46a24.09 24.09 0 013 .53l12.93-50.16 7.19-2.36a9.56 9.56 0 005.72-6.38l2.07-9.89a9.51 9.51 0 00-2.67-8.15l-7.53-6.74a9.53 9.53 0 00-8.38-1.76l-9.6 3.15a9.51 9.51 0 00-5.72 6.38l-2.07 9.89a9.44 9.44 0 002.66 8.14zm-2.71-16.17l.67-3.15a9.47 9.47 0 015.71-6.39l3.06-1a9.49 9.49 0 018.39 1.76l2.4 2.15a9.49 9.49 0 012.67 8.14l-.67 3.15a9.49 9.49 0 01-5.71 6.39l-3.06 1a9.53 9.53 0 01-8.39-1.77l-2.41-2.14a9.46 9.46 0 01-2.66-8.14zM402.62 228.74a2.73 2.73 0 00-2.44-.41l-2.73 1a2.71 2.71 0 00-1.58 1.9l-.5 2.87a2.78 2.78 0 00.86 2.32l2.24 1.86a2.71 2.71 0 002.43.41l2.74-1a2.77 2.77 0 001.58-1.9l.49-2.88a2.77 2.77 0 00-.85-2.32z"
    />
    <path
      className="icons_nunet-22_svg__cls-4"
      d="M314.47 176.05a2.85 2.85 0 002.5.53l2.86-.94a2.84 2.84 0 001.7-1.9l.62-3a2.79 2.79 0 00-.8-2.42l-2.24-2a2.81 2.81 0 00-2.5-.53l-2.86.94a2.86 2.86 0 00-1.7 1.9l-.62 3a2.86 2.86 0 00.8 2.43zM392 334.41a15.87 15.87 0 00-13.38-5.16L367.37 331l-50.73-55.68a23.4 23.4 0 01-2.74 2.23l36.7 63.82-5.88 15.26a15.91 15.91 0 002.28 14.2L357.57 384a15.9 15.9 0 0013.43 5.13l16.69-2.62a15.92 15.92 0 0011.16-9l6.07-15.78a15.89 15.89 0 00-2.22-14.16zm1.48 29.11l-1.94 5a15.86 15.86 0 01-11.15 9l-5.33.84a15.87 15.87 0 01-13.38-5.16l-3.36-4.2a15.94 15.94 0 01-2.23-14.17l1.94-5a15.86 15.86 0 0111.16-9l5.32-.84a15.88 15.88 0 0113.39 5.16l3.38 4.18a16 16 0 012.23 14.19zM415.5 223.43l-7.77-6.43a9.5 9.5 0 00-8.44-1.46l-9.48 3.46a9.49 9.49 0 00-5.49 6.58l-1.27 7.42c-10.45 3.33-44.28 14.37-60.77 19.77a21.59 21.59 0 01.7 2.57l63.72-10.83 6.66 5.53a9.46 9.46 0 008.44 1.45l9.48-3.5a9.46 9.46 0 005.47-6.58l1.72-10a9.49 9.49 0 00-2.97-7.98zm-5.12 10.37l-.47 2.72a8.15 8.15 0 01-4.7 5.67l-2.59.95a8.17 8.17 0 01-7.25-1.24l-2.13-1.77a8.19 8.19 0 01-2.55-6.9l.48-2.73a8.16 8.16 0 014.7-5.66l2.59-1a8.14 8.14 0 017.25 1.25l2.13 1.76a8.14 8.14 0 012.54 6.95z"
    />
    <path
      className="icons_nunet-22_svg__cls-4"
      d="M379.6 352.29a4.39 4.39 0 00-3.72-1.43l-4.65.72a4.43 4.43 0 00-3.11 2.51l-1.69 4.4a4.4 4.4 0 00.62 3.94l2.95 3.67a4.45 4.45 0 003.73 1.43l4.65-.73a4.38 4.38 0 003.11-2.51l1.69-4.39a4.42 4.42 0 00-.62-3.95z"
    />
    <path
      className="icons_nunet-22_svg__cls-1"
      d="M245.54 260a3.08 3.08 0 004.32.62l2.15-1.61a3.1 3.1 0 00.62-4.33l-1.63-2.11a3.09 3.09 0 00-4.32-.62l-2.14 1.62a3.08 3.08 0 00-.63 4.32z"
    />
    <path
      className="icons_nunet-22_svg__cls-1"
      d="M239.39 268.43a10 10 0 0014 2l7-5.24a8.75 8.75 0 00.77-.71l19-2.86c-.06-.65-.1-1.32-.1-2a21.07 21.07 0 01.36-3.83L263 252.07a9.32 9.32 0 00-.54-.9l-5.25-7a10 10 0 00-14-2l-7 5.24a10 10 0 00-2 14zM241 251l4.21-3.15a6 6 0 018.45 1.21l3.16 4.2a6 6 0 01-1.21 8.45l-4.21 3.16a6 6 0 01-8.45-1.21l-3.15-4.21A6 6 0 01241 251zM269.65 324.43a13.71 13.71 0 00-1.16-4.64l22.64-41.26a22.34 22.34 0 01-3.09-2.09l-31.58 35.17a12.38 12.38 0 00-1.49 0l-12 .81a13.77 13.77 0 00-12.8 14.69l.82 12a13.77 13.77 0 0014.68 12.8l12-.81a13.79 13.79 0 0012.8-14.69zm-14.9 19l-7.22.49a8.29 8.29 0 01-8.85-7.71l-.49-7.22a8.29 8.29 0 017.71-8.84l7.22-.49a8.28 8.28 0 018.84 7.71l.5 7.22a8.29 8.29 0 01-7.71 8.82zM355.94 186.24H341a17.14 17.14 0 00-17.14 17.14v15a17.39 17.39 0 00.45 3.84l-10.72 19.33a21.53 21.53 0 015.41 5.06l21.14-11.13H356a17.14 17.14 0 0017.14-17.14v-15a17.14 17.14 0 00-17.2-17.1zm9.66 29.82A12 12 0 01353.66 228h-10.4a11.94 11.94 0 01-11.94-11.94v-10.4a11.93 11.93 0 0111.94-11.94h10.4a11.94 11.94 0 0111.94 11.94z"
    />
    <rect
      className="icons_nunet-22_svg__cls-1"
      x={341.38}
      y={203.78}
      width={14.16}
      height={14.16}
      rx={4.93}
    />
    <rect
      className="icons_nunet-22_svg__cls-1"
      x={244.23}
      y={325.68}
      width={12.18}
      height={12.18}
      rx={4.24}
      transform="rotate(-3.88 250.478 331.98)"
    />
    <path
      d="M472.82 257.32a156.57 156.57 0 11-313.14 0"
      stroke="url(#icons_nunet-22_svg__linear-gradient)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
    />
    <path
      stroke="url(#icons_nunet-22_svg__linear-gradient-2)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
      d="M181.26 336.67h-39.01"
    />
    <path
      stroke="url(#icons_nunet-22_svg__linear-gradient-3)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
      d="M316.25 413.89v48.51"
    />
    <path
      stroke="url(#icons_nunet-22_svg__linear-gradient-4)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
      d="M455.42 329.12h41.39"
    />
    <path
      stroke="url(#icons_nunet-22_svg__linear-gradient-5)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
      d="M315.89 438.15h62.67"
    />
    <path
      stroke="url(#icons_nunet-22_svg__linear-gradient-6)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
      d="M476.11 329.12v36.55"
    />
    <path
      stroke="url(#icons_nunet-22_svg__linear-gradient-7)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
      d="M161.76 339.84v59.27h27.77"
    />
    <circle
      cx={192.62}
      cy={397.66}
      r={11.36}
      fill="url(#icons_nunet-22_svg__linear-gradient-8)"
    />
    <circle
      cx={386.94}
      cy={438.15}
      r={11.36}
      fill="url(#icons_nunet-22_svg__linear-gradient-9)"
    />
    <circle
      cx={316.78}
      cy={473.76}
      r={11.36}
      stroke="url(#icons_nunet-22_svg__linear-gradient-10)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
    />
    <circle
      cx={508.17}
      cy={329.12}
      r={11.36}
      stroke="url(#icons_nunet-22_svg__linear-gradient-11)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
    />
    <circle
      cx={130.89}
      cy={335.61}
      r={11.36}
      stroke="url(#icons_nunet-22_svg__linear-gradient-12)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
    />
    <circle
      cx={476.11}
      cy={373.04}
      r={11.36}
      fill="url(#icons_nunet-22_svg__linear-gradient-13)"
    />
    <path
      d="M316.78 397.66C232.4 395 181.75 343 176.53 256.87"
      stroke="url(#icons_nunet-22_svg__linear-gradient-14)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={4}
    />
  </svg>
);

export default SvgIconsnunet22;
