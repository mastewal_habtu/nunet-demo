# Nunet Demo

See feasibility of some docker containers for orchestration

# Prerequisite

https://docs.docker.com/compose/install/

# Docker-Compose Installation

To build the docker model of the applications with docker-compose

```bash
docker-compose build # building the images

docker-compose up # running server

localhost:3000/login # accessing the app

docker-compose stop # stop server

```
