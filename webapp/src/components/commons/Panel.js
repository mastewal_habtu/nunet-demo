import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

import { amber, green } from "@material-ui/core/colors";

import ErrorIcon from "@material-ui/icons/Error";
import InfoIcon from "@material-ui/icons/Info";
import WarningIcon from "@material-ui/icons/Warning";
import SuccessIcon from "@material-ui/icons/CheckCircle";

const useStyles = makeStyles(theme => ({
    icon_container: {
        textAlign: "center",
        padding: theme.spacing(2, 1, 2, 2),
        // center icon vertically
        display: "flex",
        alignItems: "center"
    },
    text_container: {
        textAlign: "left",
        padding: theme.spacing(2, 2, 2, 1)
    }
}));

const variantIcons = {
    success: SuccessIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon
};

const variantColors = {
    success: green[600],
    error: "error.dark", // theme color
    info: "primary.main", // theme color
    warning: amber[700]
};

export default function Panel(props) {
    const classes = useStyles();

    // defaults to info variant
    let variant = props.variant || "info";

    const Icon = variantIcons[variant];
    const color = variantColors[variant];

    return (
        <Box clone mt={2} mb={2} bgcolor={color} color="white">
            <Paper className={classes.paper}>
                <Grid container>
                    <Grid item xs={2} className={classes.icon_container}>
                        <Icon />
                    </Grid>
                    <Grid item xs={10} className={classes.text_container}>
                        <Typography>{props.children}</Typography>
                    </Grid>
                </Grid>
            </Paper>
        </Box>
    );
}
