import React, { useEffect, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(theme => ({
    terminal: {
        fontFamily: "Inconsolata, monospace",
        fontSize: (theme.spacing(1) / 2) * 3, // about 12px
        fontWeight: 400, // make it little bold
        lineHeight: "initial", // don't let line height intimidate terminal
        color: "white",
        backgroundColor: "black",
        padding: theme.spacing(1, 1),
        height: theme.spacing(30), // limit height of terminal
        overflow: "scroll" // scroll down when content is greater than terminal window
    },
    line: {
        margin: theme.spacing(1) / 2 //about 4px
    }
}));

export default function Terminal(props) {
    const classes = useStyles();

    const terminal = useRef(null);

    useEffect(() => {
        // scroll terminal downwards when logging if necessary
        terminal.current.scrollTop = terminal.current.scrollHeight;
    }, [props.lines]);

    return (
        <Paper
            className={classes.terminal}
            align="left"
            elevation={0}
            square
            ref={terminal}
        >   
            {props.lines.map((line, index) => (
                <p className={classes.line} key={index}>
                    {line}
                </p>
            ))}
            {/* <p className={classes.line}>Docker interactions</p>
            <p className={classes.line}>Pulling docker from github...</p>
            <p className={classes.line}>Building docker...</p> */}
        </Paper>
    );
}
